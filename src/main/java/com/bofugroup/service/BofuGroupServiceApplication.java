package com.bofugroup.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BofuGroupServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BofuGroupServiceApplication.class, args);
	}

}
