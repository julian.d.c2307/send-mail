package com.bofugroup.service.bussines.dto;

import org.springframework.stereotype.Component;

@Component
public class BofuMail {
	
	private String email;
	
	private String subject;
	
	private String text;
	
	public BofuMail() {
		super();
	}

	public BofuMail(String email, String subject, String text) {
		super();
		this.email = email;
		this.subject = subject;
		this.text = text;
	}

	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
}

