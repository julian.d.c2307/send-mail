package com.bofugroup.service.bussines.srv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.bofugroup.service.bussines.dto.BofuMail;

@Service
public class MailService {

	private JavaMailSender javaMailSender;

	@Autowired
	public MailService(JavaMailSender javaMailSender) {
		this.javaMailSender = javaMailSender;
	}
	
	public void sendEmailB(BofuMail bofuuser)throws MailException {
		SimpleMailMessage mail = new SimpleMailMessage();
		mail.setTo("*****@gmail.com");
		//mail.setFrom(bofuuser.getEmail());
		mail.setSubject(bofuuser.getSubject());
		mail.setText(bofuuser.getText() + " MY EMAIL ES :" + bofuuser.getEmail());
		javaMailSender.send(mail);
	}

	public void sendEmail(BofuMail bofuuser) throws MailException {
		SimpleMailMessage mail = new SimpleMailMessage();
		sendEmailB(bofuuser);
		mail.setTo(bofuuser.getEmail());
		mail.setFrom("*****@gmail.com");
		mail.setSubject("Respuesta");
		mail.setText("Hola Acabas de Comunicarte con BofuGroup Tu solicitud sera Atendida Muy Pronto espera respuesta");

		javaMailSender.send(mail);
	}

}
