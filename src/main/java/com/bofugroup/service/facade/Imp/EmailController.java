package com.bofugroup.service.facade.Imp;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bofugroup.service.bussines.dto.BofuMail;
import com.bofugroup.service.bussines.srv.MailService;


@RestController
public class EmailController {
	@Autowired
	private MailService notificationService;

	@PostMapping("/send-mail")
	public String send(@RequestBody BofuMail bofuuser) {
		try {
			notificationService.sendEmail(bofuuser);
		} catch (MailException mailException) {
			System.out.println(mailException);
		}
		return "Gracias Por Comunicarte con nosotros!!.";
	}

}
